//
//  PantallaCesta.swift
//  Ejemplo carro compra
//
//  Created by Francesc Roig i Feliu on 09/11/2018.
//  Copyright © 2018 Francesc Roig i Feliu. All rights reserved.
//

import UIKit


/*
 * Estos arrays estan definidos fuera del bloque principal a modo de variables
 * globales. De este modo pueden ser accedidos desde otros ficheros de código
 * como el de "PantallaLista". Si estubieran definidos dentro bloque "PantallaCesta"
 * sólo serian visibles por las funciones de este bloque.
 */
var producto_cesta:[String] = []
var precio_cesta:[Double] = []


/*
 * Inicio del bloque principal (el controlador).
 */
class PantallaCesta: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tablaCesta: UITableView!
    @IBOutlet weak var etiquetaTotal: UILabel!
    
    
    /*
     * Se ejecuta la primera vez que se carga este controlador.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tablaCesta.delegate = self
        tablaCesta.dataSource = self
    }
    
    
    /*
     * Se ejecuta cada vez que este controlador de presenta en pantalla.
     * (cada vez que se muestra la pantalla en el terminal).
     */
    override func viewWillAppear(_ animated: Bool) {
        var total:Double = 0
        
        for p in precio_cesta {
            total = total + p
        }
        
        etiquetaTotal.text = "\(total) €"
        
        if producto_cesta.count > 0 {
            let indexPath = IndexPath(row: producto_cesta.count-1, section: 0)
            tablaCesta.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    
    /*
     * Esta función es invocada automáticamente por el componente UITableView para
     * saber cuantas filas debe mostrar. El número de filas serà el número de productos.
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return producto_cesta.count
    }
    
    
    /*
     * Esta función es invocada cada vez que el componente UITableView debe construir
     * una celda. Serà invocada tantas veces como productos deba mostrar.
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celda_cesta", for: indexPath)
        cell.textLabel?.text = producto_cesta[indexPath.row]
        cell.detailTextLabel?.text = "\(precio_cesta[indexPath.row]) €"
        
        return cell
    }

    
    /*
     * Esta función es invocada por el UITableView para saber si la tabla debe
     * poder ser editada. En este caso para borra una fila.
     */
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    
    /*
     * Esta función es invocada en el momento en que se realiza la acción de borrado
     * de una fila. Permite borrar el elemento del array de productos y eliminarlo
     * de la tabla.
     */
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            producto_cesta.remove(at: indexPath.row) // Borra el elemento del array de productos.
            tablaCesta.deleteRows(at: [indexPath], with: .right) // Borra el elemento de la tabla.
        }
    }
    
} // Final bloque principal "PantallaCesta"
