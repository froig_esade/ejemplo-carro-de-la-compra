//
//  PantallaLista.swift
//  Ejemplo carro compra
//
//  Created by Francesc Roig i Feliu on 09/11/2018.
//  Copyright © 2018 Francesc Roig i Feliu. All rights reserved.
//

import UIKit

/*
 * Inicio del bloque principal (el controlador).
 */
class PantallaLista: UITableViewController {

    // En estos array debemos definir nuestra lista de productos y precio.
    var producto_lista:[String] = ["Producto 1", "Producto 2", "Producto 3"]
    var precio_lista:[Double] = [23.7, 32.78, 3.56]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    /*
     * Esta función es invocada automáticamente por el componente UITableView para
     * saber cuantas secciones contendrá la tabla. En este caso sólo una.
     */
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    
    
    /*
     * Esta función es invocada automáticamente por el componente UITableView para
     * saber cuantas filas debe mostrar. El número de filas serà el número de productos.
     */
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return producto_lista.count
    }

    
    
    /*
     * Esta función es invocada cada vez que el componente UITableView debe construir
     * una celda. Serà invocada tantas veces como productos deba mostrar.
     */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celda_producto", for: indexPath)

        cell.textLabel?.text = producto_lista[indexPath.row]
        cell.detailTextLabel?.text = "\(precio_lista[indexPath.row]) €"
        
        return cell
    }

    
    /*
     * Esta función es invocada cada vez que se selecciona un elemento de la tabla.
     * Esto permite saber que producto se ha seleccionado y, en este caso, añadirlo
     * a la lista de productos de la cesta.
     */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        producto_cesta.append(producto_lista[indexPath.row])
        precio_cesta.append(precio_lista[indexPath.row])
    }

    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
